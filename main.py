import serial
import uuid
import sys
import cv2

cap = cv2.VideoCapture(0)
ser = serial.Serial('/dev/tty.usbmodem1451', 9600)

def takePhoto(count):
    ret, frame = cap.read()
    img = str(uuid.uuid1()) + '.png';
    cv2.imwrite(img, frame)
    count += 1
    return count

def main(argv):
    try:
        maxCount = int(argv[0])
        count = 0
        while(True):
            val = ser.readline().rstrip('\r\n')
            if val == "motion":
                count = takePhoto(count)
                print count
            if count == maxCount:
                break
    except:
        print "Pass an int arg: number of images to take";

if __name__ == "__main__":
    main(sys.argv[1:])
    cap.release()
    cv2.destroyAllWindows()